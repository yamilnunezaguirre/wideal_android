<?php
  header('Access-Control-Allow-Origin: *');
  include 'vars.php';

  $conn = new mysqli($servername, $dbusername, $dbpassword, $dbname, $dbport);
// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}

$sqlb = "Select id,descripcion,data,(SELECT COUNT(id)from empresas Where sector = c.id And tipo=0 AND EstadoE=0) emps from catalogos c where parent=1 " ;
$resultb = $conn->query($sqlb);

$content = '';
if ($resultb->num_rows > 0) {
// output data of each row
while($rowb = $resultb->fetch_assoc()) {

    $content = $content . '
    <div class="news" id="enterprises">
    
          <div onclick="getEmpresas('. $rowb[id] .')" class="media-top-object animated fadeinright z-depth-1 delay-2" style="cursor:pointer">
            <a onclick="getEmpresas('. $rowb[id] .')" ><img  src="http://www.widealapp.com/admin/admin/uploads/'. $rowb[data] .'" alt="" style="width:100px"></a>
            <div class="media-body">
              <h4 class="news-title">
                <a onclick="getEmpresas('. $rowb[id] .')">Sector '. $rowb[descripcion] .'</a>
              </h4>
              <div class="card-feedback">
                <div class="card-share">
                  <span>Empresas registradas: '. $rowb[emps] .'</span>
                </div>
              </div>
            </div>
          </div>
        </div>
    ';
    }
}
$conn->close();


  echo $content;
?>



