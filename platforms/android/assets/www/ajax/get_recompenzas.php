<?php
  header('Access-Control-Allow-Origin: *');
  include 'vars.php';

  $conn = new mysqli($servername, $dbusername, $dbpassword, $dbname, $dbport);
// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}

$sqlb = "Select recompenzas.id rid,id_Emp,nombre,descripcion,puntos,recompenzas.foto lafoto,nombreE,logo 
from recompenzas INNER JOIN empresas ON empresas.id = id_Emp Order by recompenzas.id Desc" ;
$resultb = $conn->query($sqlb);

$content = '';
if ($resultb->num_rows > 0) {
// output data of each row
while($rowb = $resultb->fetch_assoc()) {
    
    
    $content = $content . '
                <div class="swiper-slide" >
              <div class="event-header">
                <div class="opacity-overlay"></div>
                <img src="http://www.widealapp.com/admin/admin/uploads/'. $rowb[lafoto] .'">
                <div class="bottom p-20" style="bottom:0px !important;margin-bottom:0px !important;padding-bottom:0px  !important">
                  <div class="social-share-author m-0 animated fadeinup delay-4">
                    <img src="http://www.widealapp.com/admin/admin/uploads/'. $rowb[logo] .'" alt="" class="avatar big">
                    <span class="white-text">'. $rowb[nombreE] .'</span>
                  </div>
                  <div class="animated fadeinup delay-2">
                    <h2 class="title white-text">'. $rowb[nombre] .'</h2>
                    <p class="white-text m-0">'. $rowb[descripcion] .'</p>
                    <span style="margin-bottom:5px; font-weight:bolder" class="event-category small white-text">Puntos: '. $rowb[puntos] .'</span>
                    <a style="margin-bottom:0px" class="waves-effect waves-light btn-large accent-color block m-b-20 animated bouncein delay-2" id="btnLogin" href=javascript:window.location="canjear.html?id='. $rowb[rid] .'">Canjear</a>
                  </div>
                </div>
              </div>
            </div>
     ';
    }
}
$conn->close();


  echo $content;
?>



