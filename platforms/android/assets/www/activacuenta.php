<?php include 'ajax/vars.php';?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Wideal Business</title>
	<base href="/">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" />
	<link href="https://fonts.googleapis.com/css?family=Montserrat:700|Roboto:400,700" rel="stylesheet">
	<!--build:css assets/css/vendor.css-->
    <link rel="shortcut icon" href="assets/img/favicon.png" />
	<link rel="stylesheet" href="bower_components/mdi/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!--endbuild-->
	<!--build:css assets/css/style.css-->
	<link rel="stylesheet" href="assets/css/icomoon.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<!--endbuild-->
</head>
<body>
    <?php 
    
    $conn = new mysqli($servername, $dbusername, $dbpassword, $dbname, $dbport);
    // Check connection	
    $sql = "
        Update usuario SET Estado = 0  Where id='".$_GET['iduser']."'
        ";
    //echo $sql;
    if ($conn->query($sql) === TRUE) {
        $last_id = $conn->insert_id;
        $OK = $last_id;
    }else{
        $OK = FALSE;
    }
    $conn->close();
    
    
    
    // Create connection
    $existsus = 0;

    $conn = new mysqli($servername, $dbusername, $dbpassword, $dbname, $dbport);
    // Check connection
    if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }

    $sqlb = "Select id_user from dealPoints Where id_user='".$_GET['iduser']."'" ;
    $resultb = $conn->query($sqlb);

    if ($resultb->num_rows > 0) {
    // output data of each row
        while($rowb = $resultb->fetch_assoc()) {
            $existsus = $existsus + 1;
        }
    }
    $conn->close();
    
    
    if ($existsus == 0){
        
            $existsa = 0;

            $conn = new mysqli($servername, $dbusername, $dbpassword, $dbname, $dbport);
            // Check connection
            if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
            }

            $sqlb = "Select id_user from dealPoints Where id_user='".$_GET['iduser']."' AND movimiento='Email validado'" ;
            $resultb = $conn->query($sqlb);

            if ($resultb->num_rows > 0) {
            // output data of each row
                while($rowb = $resultb->fetch_assoc()) {
                    $existsa = $existsa + 1;
                }
            }
        
            if ($existsa == 0){
                $conn = new mysqli($servername, $dbusername, $dbpassword, $dbname, $dbport);
                // Check connection	
                $sql = "
                INSERT INTO dealPoints (id_user,origen,movimiento,cantidad) 
                VALUES ('".$_GET['iduser']."','Email validado',0,100 )";
                //echo $sql;
                if ($conn->query($sql) === TRUE) {
                $last_id = $conn->insert_id;
                $OK = $last_id;
                }else{
                $OK = FALSE;
                }
                $conn->close(); 
            }
            
    }
    ?>
	<header class="header">
		<div class="wrap-header">
			<div class="container">
				<a href="/" class="logo">
					<img src="assets/img/logo-wideal.png" alt="Wideal Business">
				</a>
				<button class="button button-main btn-canvas hidden-md hidden-lg">
					<i class="mdi mdi-menu"></i>
				</button>
				<menu class="menu">
					<ul class="list">
						<li>
							<a href="#home">Inicio</a>
						</li>
						<li>
							<a href="#about">Nosotros</a>
						</li>
						<li>
							<a href="#how">Cómo funciona</a>
						</li>
						<li>
							<a href="#business">Empresas</a>
						</li>
						<li>
							<a href="#consumer">Usuarios</a>
						</li>
						<li>
							<a href="#contact">Contacto</a>
						</li>
						<li>
							<ol>
								<li>
									<a href="http://www.widealapp.com/admin/login.php" class="login" target="_blank">
										Ingreso
									</a>
								</li>
								<li>
									<a href="" class="register">
										Registro
									</a>
								</li>
							</ol>
						</li>
					</ul>
				</menu>
			</div>
		</div>
	</header>
	<section id="home_view">
				<div class="container" style="margin-bottom: 50px">
					<h2>
						CUENTA ACTIVA
					</h2>
					<p>ACEPTACIÓN DE LOS TÉRMINOS DE USO</p>
					<p>
						Su cuenta está activa, acaba de recibir 100 dealpoints. Puede empezar a buscar oportunidades de negocios con las empresas afiliadas a Wideal App
					</p>
					
					
				</div>
	</section>
	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="logo">
						<img src="assets/img/logo-wideal-white.png" alt="" class="img">
					</div>
				</div>
				<div class="col-sm-3">
					<ul class="list">
						<li>
							<a href="#home">Inicio</a>
						</li>
						<li>
							<a href="#how">Cómo funciona</a>
						</li>
						<li>
							<a href="#business">Empresas</a>
						</li>
						<li>
							<a href="#consumer">Usuarios</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-3">
					<ul class="list">
						<li>
							<a >Términos y condiciones</a>
						</li>
						<li>
							<a >Políticas de privacidad</a>
						</li>
						<li>
							<a href="#about">Nosotros</a>
						</li>
						<li>
							<a >Ayuda</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-3">
					<ul class="list links">
						<li>
							<a href="https://www.facebook.com/Widealapp" class="social" target="_blank">
								<i class="mdi mdi-facebook"></i>
							</a>
							<a href="https://www.instagram.com/wideal_app/" class="social" target="_blank">
								<i class="mdi mdi-instagram"></i>
							</a>
						</li>
						<li class="links-store">
							<a  class="store">
								<img src="assets/img/google-play.png" alt="" class="img">
							</a>
						</li>
						<li class="links-store">
							<a  class="store">
								<img src="assets/img/app-store.png" alt="" class="img">
							</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-12">
					<p class="copyright">2017 WIDEAL®. All rights reserved <strong>Design by: <a target="_blank" href="http://skycom.ec">SKYCOM</a></strong></p>
				</div>
			</div>
		</div>
	</footer>
	<!--build:js assets/js/vendor.js-->
	<script src="bower_components/jquery/dist/jquery.min.js"></script>
	<script src="bower_components/moment/min/moment.min.js"></script>
	<script src="bower_components/moment/min/moment-with-locales.min.js"></script>
	<!--endbuild-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDR0ay1pAa7HSNnNKMyvDKIRSKaL3jEooM&callback=initMap"
    async defer></script>
	<script>
		var map;
		function initMap() {
			map = new google.maps.Map(document.getElementById('map'), {
				center: {lat: -2.167329, lng:-79.8960551 },
          		streetViewControl: false,
				mapTypeControlOptions: {
					style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
					position: google.maps.ControlPosition.TOP_CENTER
				},
				zoomControlOptions: {
					position: google.maps.ControlPosition.RIGHT_CENTER
				},
				zoom: 14
			});
		}
		
		$("#text1").show();
		$("#text2").hide();	
		$("#text3").hide();	
		$("#text4").hide();	
				
		function habilitatexto(id){
			
			if(id==1){				
				$("#text1").show();
				$("#text2").hide();	
				$("#text3").hide();	
				$("#text4").hide();		
			}
			if(id==2){
				$("#text1").hide();
				$("#text2").show();	
				$("#text3").hide();	
				$("#text4").hide();		
			}
			if(id==3){
				$("#text1").hide();
				$("#text2").hide();	
				$("#text3").show();	
				$("#text4").hide();		
			}
			if(id==4){
				$("#text1").hide();
				$("#text2").hide();	
				$("#text3").hide();	
				$("#text4").show();		
			}
		}
	</script>
	<script src="assets/js/script.js"></script>
</body>
</html>